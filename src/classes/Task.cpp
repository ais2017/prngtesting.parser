#include "Task.h"

Task::Task()
{
    i_file = "";
    o_file = "";
    task_descr = TaskDescr();
}

Task::Task(string i_file_,string o_file_,const TaskDescr& task_){
    i_file = i_file_;
    o_file = o_file_;
    task_descr = task_;
}

Task::Task(const Task& other)
{
    i_file = other.i_file;
    o_file = other.o_file;
    task_descr = other.task_descr;
}

Task::~Task()
{
    //dtor
}

/*
std::ostream& Task::meta_print(std::ostream& out){
 }
    out<<"Task descriptor|Input file:"<<i_file
        <<"|Output file:"<<o_file
        <<"Sequence format:";
        task_descr.meta_print(out);
    return out;


 std::istream& Task::stream_load(std::istream& in){
        in>>i_file>>o_file;
        task_descr.stream_load(in);
        return in;

 }*/

 bool operator==(const Task& a, const Task& b)
 {
    return ( a.i_file == b.i_file &&
             a.o_file == b.o_file &&
             a.task_descr == b.task_descr );
 }

