#ifndef TASKDESCR_H
#define TASKDESCR_H

#include <iostream>
#include <vector>
#include <stddef.h>
#include <cstddef>

class TaskDescr
{
     private:
        ///root sequence attributes
        size_t allocated_data; //data volume in bytes, which will be parsed from file
        size_t initial_step; //number of starting bits, which will be skipped
        size_t elem_ct; //number of subsequences
        char   seq_separator; //symbol, separating subsequences
        ///subsequence attributes
        size_t elem_width; //number of bits in subsequence element
        size_t subseq_elem_ct; //number of elements in subsequence
        size_t subseq_beg_step; //number of bits between first bits of subsequence elements
        size_t subseq_end_step; //number of bits between first bits of first elements of two consequent subsequences
        char subseq_separator; //symbol, separating elements in subsequences

    public:
        TaskDescr();

        TaskDescr(size_t allocated_data_,
        size_t initial_step_,
        size_t elem_ct_,
        char   seq_separator_,
        size_t elem_width_,
        size_t subseq_elem_ct_,
        size_t subseq_beg_step_,
        size_t subseq_end_step_,
        char subseq_separator_);

        TaskDescr(const TaskDescr&);
        ~TaskDescr();

        //getters
        size_t get_allocated_data() { return allocated_data; }
        size_t get_initial_step() { return initial_step; }
        size_t get_elem_ct() { return elem_ct; }
        char get_seq_separator() { return seq_separator; }
        size_t get_elem_width() { return elem_width; }
        size_t get_subseq_elem_ct() {return subseq_elem_ct; }
        size_t get_subseq_beg_step() { return subseq_beg_step; }
        size_t get_subseq_end_step() { return subseq_end_step; }
        char get_subseq_separator() {return subseq_separator; }

        //setters
        int set_allocated_data(size_t);
        int set_initial_step(size_t);
        int set_elem_ct(size_t);
        int set_seq_separator(char);
        int set_elem_width(size_t);
        int set_subseq_elem_ct(size_t);
        int set_subseq_beg_step(size_t);
        int set_subseq_end_step(size_t);
        int set_subseq_separator(char);

        // I/O
        //std::ostream& meta_print(std::ostream& out);
        //std::istream& stream_load(std::istream& in);

        friend bool operator==(const TaskDescr& a, const TaskDescr& b);
};




#endif // TASKDESCR_H
