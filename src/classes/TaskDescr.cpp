#include "TaskDescr.h"
#include <exception>
/* There is only one coherency rule: boundary check.
    (elem_ct-1)*subseq_end_step+(subseq_elem_ct-1)*subseq_beg_step+elem_width <= allocated_data*8
    */

using namespace std;
TaskDescr::TaskDescr(){
        allocated_data = 0; //data volume in bytes, which will be parsed from file
        initial_step = 0; //number of starting bits, which will be skipped
        elem_ct = 0; //number of subsequences
        seq_separator = '|'; //symbol, separating subsequences
        elem_width = 0; //number of bits in subsequence element
        subseq_elem_ct = 0; //number of elements in subsequence
        subseq_beg_step = 0; //number of bits between first bits of subsequence elements
        subseq_end_step = 0; //number of bits between first bits of first elements of two consequent subsequences
        subseq_separator = ','; //symbol, separating elements in subsequences
}

TaskDescr::TaskDescr(const TaskDescr &val){
        allocated_data = val.allocated_data; //data volume in bytes, which will be parsed from file
        initial_step = val.initial_step; //number of starting bits, which will be skipped
        elem_ct = val.elem_ct; //number of subsequences
        seq_separator = val.seq_separator; //symbol, separating subsequences
        elem_width = val.elem_width; //number of bits in subsequence element
        subseq_elem_ct = val.subseq_elem_ct; //number of elements in subsequence
        subseq_beg_step = val.subseq_beg_step; //number of bits between first bits of subsequence elements
        subseq_end_step = val.subseq_end_step; //number of bits between first bits of first elements of two consequent subsequences
        subseq_separator = val.subseq_separator; //symbol, separating elements in subsequences
}

TaskDescr::TaskDescr(size_t allocated_data_,
        size_t initial_step_,
        size_t elem_ct_,
        char   seq_separator_,
        size_t elem_width_,
        size_t subseq_elem_ct_,
        size_t subseq_beg_step_,
        size_t subseq_end_step_,
        char subseq_separator_ ){
        if((elem_ct_-1)*subseq_end_step_+(subseq_elem_ct_-1)*subseq_beg_step_+elem_width_ <= allocated_data_*8){
            allocated_data = allocated_data_;
            initial_step = initial_step_;
            elem_ct = elem_ct_;
            seq_separator = seq_separator_;
            elem_width = elem_width_;
            subseq_elem_ct = subseq_elem_ct_;
            subseq_beg_step = subseq_beg_step_;
            subseq_end_step = subseq_end_step_;
            subseq_separator = subseq_separator_;
        } else {
            throw "Illegal set of values";
        }
}



TaskDescr::~TaskDescr()
{
        allocated_data = 0; //data volume in bytes, which will be parsed from file
        initial_step = 0; //number of starting bits, which will be skipped
        elem_ct = 0; //number of subsequences
        seq_separator = '|'; //symbol, separating subsequences
        elem_width = 0; //number of bits in subsequence element
        subseq_elem_ct = 0; //number of elements in subsequence
        subseq_beg_step = 0; //number of bits between first bits of subsequence elements
        subseq_end_step = 0; //number of bits between first bits of first elements of two consequent subsequences
        subseq_separator = ','; //symbol, separating elements in subsequences
}

int TaskDescr::set_allocated_data(size_t val){
    if( ((elem_ct-1)*subseq_end_step+(subseq_elem_ct-1)*subseq_beg_step+elem_width) <= val*8){
        allocated_data = val;
        return 0;
    }
    return -1;
}

int TaskDescr::set_initial_step(size_t val){ //file-related check, done on control side before descriptor creation
    initial_step = val;
    return 0;
}

int TaskDescr::set_elem_ct(size_t val){
    if( ((val-1)*subseq_end_step+(subseq_elem_ct-1)*subseq_beg_step+elem_width) <= allocated_data*8){
        elem_ct = val;
        return 0;
    }
    return -1;
}

int TaskDescr::set_seq_separator(char val){
    seq_separator = val;
    return 0;
}

int TaskDescr::set_subseq_separator(char val){
    subseq_separator = val;
    return 0;
}

int TaskDescr::set_elem_width(size_t val){
    if((elem_ct-1)*subseq_end_step+(subseq_elem_ct-1)*subseq_beg_step+val <= allocated_data*8){
        elem_width = val;
        return 0;
    }
    return -1;
}

int TaskDescr::set_subseq_elem_ct(size_t val){
    if((elem_ct-1)*subseq_end_step+(val-1)*subseq_beg_step+elem_width <= allocated_data*8){
        subseq_elem_ct = val;
        return 0;
    }
    return -1;
}

int TaskDescr::set_subseq_beg_step(size_t val){
    if((elem_ct-1)*subseq_end_step+(subseq_elem_ct-1)*val+elem_width <= allocated_data*8){
        subseq_beg_step = val;
        return 0;
    }
    return -1;
}

int TaskDescr::set_subseq_end_step(size_t val){
    if((elem_ct-1)*val+(subseq_elem_ct-1)*subseq_beg_step+elem_width <= allocated_data*8){
        subseq_end_step = val;
        return 0;
    }
    return -1;
}
/*
 std::ostream& TaskDescr::meta_print(std::ostream& out){
     int ct=allocated_data*8-((elem_ct-1)*subseq_end_step+(subseq_elem_ct-1)*subseq_beg_step+elem_width);
    out<<"Allocated data:"<<allocated_data<<"|"
        <<"Initial bits skipped:"<<initial_step<<"|"
        <<"# of subsequences:"<<elem_ct<<"|"
        <<"Subsequneces separator:"<<seq_separator<<endl<<"----"
        <<"Element bit width:"<<elem_width<<"|"
        <<"# of elements in subsequence:"<<subseq_elem_ct<<"|"
        <<"Bits between element first bits:"<<subseq_beg_step<<"|"
        <<"Bits between first bits of first elements of subsequences:"<<subseq_end_step<<"|"
        <<"Element separator:"<<subseq_separator<<"|"
        <<"Unused allocated bits(bytes):"<<ct<<"("<<ct/8<<")"<<endl;
    return out;
 }


 std::istream& TaskDescr::stream_load(std::istream& in){
        size_t allocated_data_;
        size_t initial_step_;
        size_t elem_ct_;
        char   seq_separator_;
        size_t elem_width_;
        size_t subseq_elem_ct_;
        size_t subseq_beg_step_;
        size_t subseq_end_step_;
        char subseq_separator_;
        in>>allocated_data_>>initial_step_>>elem_ct_>>seq_separator_>>elem_width_
        >>subseq_elem_ct_>>subseq_beg_step_>>subseq_end_step_>>subseq_separator_;
        if((elem_ct_-1)*subseq_end_step_+(subseq_elem_ct_-1)*subseq_beg_step_+elem_width_ <= allocated_data_*8){
            allocated_data = allocated_data_;
            initial_step = initial_step_;
            elem_ct = elem_ct_;
            seq_separator = seq_separator_;
            elem_width = elem_width_;
            subseq_elem_ct = subseq_elem_ct_;
            subseq_beg_step = subseq_beg_step_;
            subseq_end_step = subseq_end_step_;
            subseq_separator = subseq_separator_;
        }
        return in;
 }*/


 bool operator==(const TaskDescr& a, const TaskDescr& b)
 {
    return (    a.allocated_data == b.allocated_data &&
                a.initial_step == b.initial_step &&
                a.elem_ct == b.elem_ct &&
                a.seq_separator == b.seq_separator &&
                a.elem_width == b.elem_width &&
                a.subseq_elem_ct == b.subseq_elem_ct &&
                a.subseq_beg_step == b.subseq_beg_step &&
                a.subseq_end_step == b.subseq_end_step &&
                a.subseq_separator == b.subseq_separator);

 }
