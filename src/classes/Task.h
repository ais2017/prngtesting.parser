#ifndef TASK_H
#define TASK_H
#include <string>
#include <TaskDescr.h>

using namespace std;

class Task
{
    private:
        string i_file;  //input filename
        string o_file;  //output filename
        TaskDescr task_descr; //relevant task descriptor

    public:
        Task();
        ~Task();
        Task(string,string,const TaskDescr&);
        Task(const Task& other);

        string get_i_file() { return i_file; }
        string get_o_file() { return o_file; }
        TaskDescr get_task_descr() { return task_descr; }

        int set_i_file(string val) { i_file = val; return 0; }
        int set_o_file(string val) { o_file = val; return 0; }
        int set_task_descr(TaskDescr val) { task_descr = TaskDescr(val); return 0;  }

        /*std::ostream& meta_print(std::ostream& out);
        std::istream& stream_load(std::istream& in);*/

        friend bool operator==(const Task& a, const Task& b);
};

#endif // TASK_H
