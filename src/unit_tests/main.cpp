#include <iostream>
#include <Task.h>
#include <TaskDescr.h>
#include <gtest/gtest.h>
using namespace std;
const int RANDOM_SEED = 2;
const int RANDOM_THRESHOLD = 100000;


TEST(TaskDescr,Empty_constructor){
    TaskDescr td;
    bool cond = td.get_allocated_data() == 0 &&
                td.get_initial_step() == 0 &&
                td.get_elem_ct() == 0 &&
                td.get_seq_separator() == '|' &&
                td.get_elem_width() == 0 &&
                td.get_subseq_elem_ct() == 0 &&
                td.get_subseq_beg_step() == 0 &&
                td.get_subseq_end_step() == 0 &&
                td.get_subseq_separator() == ',';
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,Value_constructor_success){
    TaskDescr td(2,0,2,',',8,1,0,8,',');
    bool cond = td.get_allocated_data() == 2&&
            td.get_initial_step() == 0&&
            td.get_elem_ct() == 2&&
            td.get_seq_separator() == ','&&
            td.get_elem_width() == 8&&
            td.get_subseq_elem_ct()==1&&
            td.get_subseq_beg_step() ==0&&
            td.get_subseq_end_step()==8&&
            td.get_subseq_separator()==',';
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,Value_constructor_allocated_data_fail){
        ASSERT_ANY_THROW(TaskDescr(1,0,2,',',8,1,0,8,','));
}

TEST(TaskDescr,Value_constructor_elem_ct_fail){
        ASSERT_ANY_THROW(TaskDescr(2,0,3,',',8,1,0,8,','));
}

TEST(TaskDescr,Value_constructor_elem_width_fail){
        ASSERT_ANY_THROW(TaskDescr(2,0,2,',',9,1,0,8,','));
}
TEST(TaskDescr,Value_constructor_subseq_elem_ct_fail){
        ASSERT_ANY_THROW(TaskDescr(2,0,2,',',8,2,1,8,','));
}

TEST(TaskDescr,Value_constructor_subseq_beg_step_fail){
        ASSERT_ANY_THROW(TaskDescr(4,0,2,',',8,2,9,16,','));
}

TEST(TaskDescr,Value_constructor_subseq_end_step_fail){
        ASSERT_ANY_THROW(TaskDescr(4,0,2,',',8,2,8,17,','));
}

TEST(TaskDescr,Copy_constructor){
    TaskDescr td1;
    TaskDescr td2(td1);
    ASSERT_EQ( td1,td2 );
}

TEST(TaskDescr,allocated_data_setter_success){
    TaskDescr td;
    size_t val = rand();
    int ret = td.set_allocated_data(val);
    bool cond = (ret==0)&&(td.get_allocated_data() == val);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,allocated_data_setter_fail){
    TaskDescr td = TaskDescr(2,0,1,',',16,1,0,0,',');
    int ret = td.set_allocated_data(1);
    bool cond = (td.get_allocated_data()==2 && ret==-1);
    ASSERT_EQ( cond,true );
}

TEST(TaskDescr,initial_step_setter){
    size_t init = rand();
    TaskDescr td;
    int ret = td.set_initial_step(init);
    ASSERT_EQ( (td.get_initial_step() == init)&&(ret==0),true );
}

TEST(TaskDescr,seq_separator_setter){
    char seq_sep = rand()%256;
    TaskDescr td;
    int ret = td.set_seq_separator(seq_sep);
    ASSERT_EQ( (td.get_seq_separator() == seq_sep)&&(ret==0),true );
}

TEST(TaskDescr,subseq_separator_setter){
    char seq_sep = rand()%256;
    TaskDescr td;
    int ret = td.set_subseq_separator(seq_sep);
    ASSERT_EQ( (td.get_subseq_separator() == seq_sep)&&(ret==0),true );
}

TEST(TaskDescr,elem_ct_setter_fail){
    TaskDescr td(2,0,2,',',8,1,0,8,',');
    int ret = td.set_elem_ct(3);
    bool cond = (td.get_elem_ct()==2 && ret==-1);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,elem_ct_setter_success)
{
    TaskDescr td(2,0,2,',',8,1,0,8,',');
    int ret = td.set_elem_ct(1);
    bool cond = (td.get_elem_ct()==1 && ret==0);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,elem_width_setter_fail){
    TaskDescr td(2,0,2,',',8,1,0,8,',');
    int ret = td.set_elem_width(9);
    bool cond = (td.get_elem_width()==8 && ret==-1);
    ASSERT_EQ(cond,true);
}


TEST(TaskDescr,elem_width_setter_success){
    TaskDescr td(2,0,2,',',8,1,0,8,',');
    int ret = td.set_elem_width(7);
    bool cond = cond&&(td.get_elem_width()==7 && ret==0);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,subseq_elem_ct_setter_fail){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_elem_ct(3);
    bool cond = (td.get_subseq_elem_ct()==2 && ret==-1);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,subseq_elem_ct_setter_success){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_elem_ct(1);
    bool cond = (td.get_subseq_elem_ct()==1 && ret==0);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,subseq_beg_step_setter_fail){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_beg_step(9);
    bool cond = (td.get_subseq_beg_step()==8 && ret==-1);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,subseq_beg_step_setter_success){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_beg_step(7);
    bool cond = cond&&(td.get_subseq_beg_step()==7 && ret==0);
    ASSERT_EQ(cond,true);
}

TEST(TaskDescr,subseq_end_step_setter_fail){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_end_step(17);
    bool cond = (td.get_subseq_end_step()==16 && ret==-1);
    ASSERT_EQ(cond,true);
}
TEST(TaskDescr,subseq_end_step_setter_success){
    TaskDescr td(4,0,2,',',8,2,8,16,',');
    int ret = td.set_subseq_end_step(15);
    bool cond = cond&&(td.get_subseq_end_step()==15 && ret==0);
    ASSERT_EQ(cond,true);
}

TEST(Task,Empty_constructor){
    Task tsk;
    TaskDescr td;
    bool cond = (tsk.get_i_file() == "" &&
                 tsk.get_o_file() == "" &&
                 tsk.get_task_descr() == td);
    ASSERT_EQ(cond,true);
}

TEST(Task,Copy_constructor){
    Task tsk1;
    Task tsk2(tsk1);
    ASSERT_EQ(tsk1,tsk2);
}

TEST(Task,Value_constructor){
    string i_file, o_file;
    TaskDescr td;
    Task tsk(i_file,o_file,td);
    bool cond = tsk.get_i_file() == i_file &&
                tsk.get_o_file() == o_file &&
                tsk.get_task_descr() == td;
    ASSERT_EQ(cond,true);
}

TEST(Task,i_file_setter){
    string i_file = "~/ifile";
    Task tsk;
    tsk.set_i_file(i_file);
    ASSERT_EQ( (tsk.get_i_file()==i_file), true);
}

TEST(Task,o_file_setter){
    string o_file = "~/ifile";
    Task tsk;
    tsk.set_o_file(o_file);
    ASSERT_EQ(tsk.get_o_file()==o_file,true);
}

TEST(Task,task_descr_setter){
    TaskDescr td;
    Task tsk;
    tsk.set_task_descr(td);
    ASSERT_EQ(tsk.get_task_descr()==td,true);
}

int main( int argc, char* argv[] )
{
    ::testing::InitGoogleTest( &argc,argv );
    return RUN_ALL_TESTS();
}
